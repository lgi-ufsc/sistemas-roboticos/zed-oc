#ifndef __ZED_OC_h__
#define __ZED_OC_h__

#define VIDEO_MOD_AVAILABLE
#define SENSORS_MOD_AVAILABLE

#include <zed-open-capture/videocapture.hpp>

// Building requires
// pkg-config --cflags --libs hidapi-libusb
#include <zed-open-capture/sensorcapture.hpp>
#include <opencv2/opencv.hpp>
#include <string>
#include <map>
#include <vector>

namespace zed
{
    using namespace sl_oc;

    std::map<std::string, video::RESOLUTION> string_resolution
    {
        {"HD2K", video::RESOLUTION::HD2K},       /**< 2208*1242, available framerates: 15 fps.*/
        {"HD1080", video::RESOLUTION::HD1080},     /**< 1920*1080, available framerates: 15, 30 fps.*/
        {"HD720", video::RESOLUTION::HD720},      /**< 1280*720, available framerates: 15, 30, 60 fps.*/
        {"VGA", video::RESOLUTION::VGA},        /**< 672*376, available framerates: 15, 30, 60, 100 fps.*/
        {"LAST", video::RESOLUTION::LAST}
    };

    std::map<video::RESOLUTION, std::vector<video::FPS>> resolution_fps
    {
        {video::RESOLUTION::HD2K, std::vector<video::FPS> {video::FPS::FPS_15}} ,       /**< 2208*1242, available framerates: 15 fps.*/
        {video::RESOLUTION::HD1080, std::vector<video::FPS> {video::FPS::FPS_15, video::FPS::FPS_30}},     /**< 1920*1080, available framerates: 15, 30 fps.*/
        {video::RESOLUTION::HD720, std::vector<video::FPS> {video::FPS::FPS_15, video::FPS::FPS_30, video::FPS::FPS_60}},      /**< 1280*720, available framerates: 15, 30, 60 fps.*/
        {video::RESOLUTION::VGA, std::vector<video::FPS> {video::FPS::FPS_15 ,video::FPS::FPS_30, video::FPS::FPS_60, video::FPS::FPS_100}}        /**< 672*376, available framerates: 15, 30, 60, 100 fps.*/
    };

    struct Calibration
    {
        float l_fx, r_fx, l_fy, r_fy = 1;
        float l_cx, r_cx, l_cy, r_cy = 0;
        float l_k1, r_k1 = 0;
        float l_k2, r_k2 = 0;
        float l_k3, r_k3 = 0;
        float l_p1, r_p1 = 0;
        float l_p2, r_p2 = 0;

        float base_line = 120;
        float ty, tz = 0;
        float cv = 0;
        float rx, rz = 0;

        cv::Mat l_cameraMatrix;
        cv::Mat r_cameraMatrix;
        cv::Mat l_cameraMatrix_rectified;
        cv::Mat r_cameraMatrix_rectified;
        cv::Mat l_mapx, l_mapy;
        cv::Mat r_mapx, r_mapy;
    };

    class ZED2 : public video::VideoCapture, public sensors::SensorCapture
    {
    public:
        using video::VideoCapture::getSerialNumber;
        using video::VideoCapture::getLastFrame;

        ZED2(video::VideoParams params = video::VideoParams(), VERBOSITY verbose_lvl = VERBOSITY::ERROR);
        ~ZED2();

        bool calibrate();
        bool calibrate(cv::String &path);

        bool initializeVideo( int devId=-1 );
        bool isInitialized(void) { return _initialized; }
        
        bool getLastFrame(cv::Mat &left_frame, cv::Mat &right_frame, bool rectify = true, uint64_t timeout_msec=10);

        Calibration calibration;

    protected:
        bool _initialized;

    private:
        bool _initCalibration(cv::String file);
    };
}

#endif