#include <zed_oc/zed_oc.h>
#include <zed_oc/calibration.hpp>
#include <iostream>

using namespace zed;

ZED2::ZED2(video::VideoParams params, VERBOSITY verbose_lv): video::VideoCapture(params), sensors::SensorCapture(verbose_lv)
{ 
    _initialized = false;
}

ZED2::~ZED2() {}

bool ZED2::_initCalibration(cv::String file)
{
    int w,h;
    getFrameSize(w,h);
    cv::Size2i image_size = cv::Size(w/2,h);

    ConfManager camerareader(file.c_str());
    if (!camerareader.isOpened())
        return 0;

    std::string resolution_str;
    switch ((int) image_size.width) {
        case 2208:
            resolution_str = "2k";
            break;
        case 1920:
            resolution_str = "fhd";
            break;
        case 1280:
            resolution_str = "hd";
            break;
        case 672:
            resolution_str = "vga";
            break;
        default:
            resolution_str = "hd";
            break;
    }

    calibration.base_line = camerareader.getValue("stereo:baseline", 0.0f);
    calibration.ty = camerareader.getValue("stereo:ty_" + resolution_str, 0.f);
    calibration.tz = camerareader.getValue("stereo:tz_" + resolution_str, 0.f);

    // Get left parameters
    calibration.l_cx = camerareader.getValue("left_cam_" + resolution_str + ":cx", 0.0f);
    calibration.l_cy = camerareader.getValue("left_cam_" + resolution_str + ":cy", 0.0f);
    calibration.l_fx = camerareader.getValue("left_cam_" + resolution_str + ":fx", 0.0f);
    calibration.l_fy = camerareader.getValue("left_cam_" + resolution_str + ":fy", 0.0f);
    calibration.l_k1 = camerareader.getValue("left_cam_" + resolution_str + ":k1", 0.0f);
    calibration.l_k2 = camerareader.getValue("left_cam_" + resolution_str + ":k2", 0.0f);
    calibration.l_p1 = camerareader.getValue("left_cam_" + resolution_str + ":p1", 0.0f);
    calibration.l_p2 = camerareader.getValue("left_cam_" + resolution_str + ":p2", 0.0f);
    calibration.l_k3 = camerareader.getValue("left_cam_" + resolution_str + ":k3", 0.0f);

    // Get right parameters
    calibration.r_cx = camerareader.getValue("right_cam_" + resolution_str + ":cx", 0.0f);
    calibration.r_cy = camerareader.getValue("right_cam_" + resolution_str + ":cy", 0.0f);
    calibration.r_fx = camerareader.getValue("right_cam_" + resolution_str + ":fx", 0.0f);
    calibration.r_fy = camerareader.getValue("right_cam_" + resolution_str + ":fy", 0.0f);
    calibration.r_k1 = camerareader.getValue("right_cam_" + resolution_str + ":k1", 0.0f);
    calibration.r_k2 = camerareader.getValue("right_cam_" + resolution_str + ":k2", 0.0f);
    calibration.r_p1 = camerareader.getValue("right_cam_" + resolution_str + ":p1", 0.0f);
    calibration.r_p2 = camerareader.getValue("right_cam_" + resolution_str + ":p2", 0.0f);
    calibration.r_k3 = camerareader.getValue("right_cam_" + resolution_str + ":k3", 0.0f);

    if (calibration.r_k1 == 0 && calibration.l_k1 == 0 && calibration.l_k2 == 0 && calibration.r_k2 == 0)
    {
        std::cerr << "Safety check failed. Wrong \'.\' or \',\' reading in file conf.\n"
            << "Delete the file and try downloading it again or try to solve it youself."
            << std::endl;
        return false;
    }

    calibration.rx =  camerareader.getValue("stereo:rx_" + resolution_str, 0.f);
    calibration.cv =  camerareader.getValue("stereo:cv_" + resolution_str, 0.f);
    calibration.rz =  camerareader.getValue("stereo:rz_" + resolution_str, 0.f);

    // Rotation Matrix
    cv::Mat R_zed = (cv::Mat_<double>(1,3) << calibration.rx, calibration.cv, calibration.rz);
    cv::Mat R;

    cv::Rodrigues(R_zed, R);

    cv::Mat distCoeffs_left, distCoeffs_right;

    // Left
    calibration.l_cameraMatrix = (cv::Mat_<double>(3, 3) << calibration.l_fx, 0, calibration.l_cx, 0, calibration.l_fy, calibration.l_cy, 0, 0, 1);
    distCoeffs_left = (cv::Mat_<double>(5, 1) << calibration.l_k1, calibration.l_k2, calibration.l_p1, calibration.l_p2, calibration.l_k3);

    // Right
    calibration.r_cameraMatrix = (cv::Mat_<double>(3, 3) << calibration.r_fx, 0, calibration.r_cx, 0, calibration.r_fy, calibration.r_cy, 0, 0, 1);
    distCoeffs_right = (cv::Mat_<double>(5, 1) << calibration.r_k1, calibration.r_k2, calibration.r_p1, calibration.r_p2, calibration.r_k3);

    // Stereo
    cv::Mat T = (cv::Mat_<double>(3,1) << calibration.base_line, calibration.ty, calibration.tz);

    cv::Mat R1, R2, P1, P2, Q;
    cv::stereoRectify(calibration.l_cameraMatrix, distCoeffs_left, calibration.r_cameraMatrix, distCoeffs_right, image_size, R, T,
            R1, R2, P1, P2, Q, cv::CALIB_ZERO_DISPARITY, 0, image_size);

    //Precompute maps for cv::remap()
    initUndistortRectifyMap(calibration.l_cameraMatrix, distCoeffs_left, R1, P1, image_size, CV_32FC1, calibration.l_mapx, calibration.l_mapy);
    initUndistortRectifyMap(calibration.r_cameraMatrix, distCoeffs_right, R2, P2, image_size, CV_32FC1, calibration.r_mapx, calibration.r_mapy);

    calibration.l_cameraMatrix_rectified = P1;
    calibration.r_cameraMatrix_rectified = P2;

    return true;
}

bool ZED2::calibrate(cv::String &path)
{
    int serial_number = this->video::VideoCapture::getSerialNumber();

    if(serial_number == -1)
    {
        std::cerr << "Could not get serial number. Check if the camera is initialized." << std::endl;
        return false;
    }

    if(!downloadCalibrationFile(serial_number, path))
    {
        std::cerr << "Could not load calibration file from StereoLabs servers." << std::endl;
        return false;
    }
    std::cout << "Calibration file's path: " << std::endl;
    std::cout << path << std::endl;

    return _initCalibration(path);
}

bool ZED2::calibrate()
{
    cv::String path;
    return calibrate(path);
}

bool ZED2::getLastFrame(cv::Mat &left_frame, cv::Mat &right_frame, bool rectify, uint64_t timeout_msec)
{
    video::Frame frame = this->video::VideoCapture::getLastFrame(timeout_msec);

    if(frame.data==nullptr) return false;

    cv::Mat full_frame;

    // Converta a imagem completa da camera para uma matriz OpenCV colorida
    cv::cvtColor(cv::Mat(frame.height, frame.width, CV_8UC2, frame.data), 
                    full_frame, cv::COLOR_YUV2BGR_YUYV);
    
    // Separe as imagens esquerda e direita
    left_frame = full_frame(cv::Range::all(), cv::Range(0,frame.width/2));
    right_frame = full_frame(cv::Range::all(), cv::Range(frame.width/2,frame.width));

    if(rectify && !calibration.r_mapx.empty())
    {
        cv::remap(left_frame, left_frame, calibration.l_mapx, calibration.l_mapy, cv::INTER_LINEAR);
        cv::remap(right_frame, right_frame, calibration.r_mapx, calibration.r_mapy, cv::INTER_LINEAR);
    }

    return true;
}

bool ZED2::initializeVideo( int devId )
{
    if( !this->video::VideoCapture::initializeVideo(devId) )
    {
        _initialized = false;
        return false;
    }
    else
    {
        _initialized = true;
        return true;
    }
}